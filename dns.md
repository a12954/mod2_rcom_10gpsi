# DNS
DNS é um sistema de dominios que permite um dominio como `site.com` ira para o ip address do servidor,


## Exemplo
Um user entra no browser e entra no site `stackoverflow.com`, o user ira enviar um pedido de dominio ao servidor DNS, que ira procurar esse dominio na cache e retornar com o ip adress, se esse servidor DNS não encontrar o domain na cache, ira fazer um pedido ao "RESOLVER" (DNS server da operadora da rede) que ira fazer o mesmo processo para encontrar o dominio, e se não encontrar o domain na cache, ele ira fazer um pedido ao "ROOT SERVER" que se não encontrar ira retornar com o TLD server (Dominio de nivel superior), e se o TLD não encontrar esse dominio ele ira retornar o "NAME SERVER", (este servidor é responsavel de saber tudo sobre o dominio,
eles são o ultimo server para responder o dominio) e ira retornar `151.101.1.69`

para isto ser possivel existem DNS servers que guardam uma lista de dominios com o ip do servidor, um exemplo de um dns server `8.8.8.8` (DNS Server da Google),
DNS server é automaticamente configurado pelo DHCP server da rede.