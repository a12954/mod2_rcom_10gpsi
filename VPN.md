# VPN
VPN (*Virtual Private Network*), permite um cliente connectar-se a uma network privada. Vpn criar uma conexão encriptada (VPN Tunnel) fazendo com que todo o tráfego e comunicação passe pelo o tunnel.

## VPN Tunnel
VPN Tunnel (Virtual Private Network) é um caminho privado que dividide e encapsula (fazendo uma nova camanda de dados) em pacotes todos os dados passados pelo túnel, tornando impossível (até para a ISP) o acesso aos pacotes originais de dados passado pelo tunnel, após os dados chegarem ao destino, essa camada de dados criada, será removida, para se poder ter acesso aos dados originais.

Criptografia usada no tunnel é escolhida pelo protocol.

Este tunnel permite trafico por os 2 lados, (client-server, server-client), protegendo de ataques MITM (Man In The Midle).

![Img](https://sc1.checkpoint.com/documents/R76/CP_R76_VPN_AdminGuide/34341.gif)
> *Figura de VPN Tunnel protocol*
	

## Remote Access VPN
VPN remoto acesso é um protocol que permite a um computador conectar-se a uma rede privada através da Internet. Ele fornece uma conexão segura entre o computador e a rede, protegendo as informações transmitidas. VPN remoto acesso também permite que os clientes acessem a rede privada de qualquer lugar que tenha uma conexão à Internet.

![Img](https://www.researchgate.net/profile/Zornitsa-Yakova/publication/256843676/figure/fig2/AS:297856234737671@1448025868760/Remote-access-VPN-1.png)
> *Figura de Remote Access VPN no Protocol OpenVPN*
	

## Site to Site VPN / Router-to-Router VPN
Um router-to-router VPN (Virtual Private Network) é um método de conectar dois roteadores através de uma rede privada virtual, permitindo aos seus clientes acessar recursos compartilhados em ambos os roteadores, como arquivos, impressoras e servidores, mesmo se eles estão fisicamente separados. 
Por exemplo, permitir que os clientes remotos acessem os recursos do escritório.

![Img](https://openvpn.net/wp-content/uploads/vpn_server_resources/layer-3-routing-diagram-step-7.png)
> *Figura de Site to Site VPN no Protocol OpenVPN*


## E2EE (End-to-End Encryption)
End-to-end encryption é um protocol de criptografia que protege os dados ao longo do seu caminho, desde o ponto de origem até o seu destino. Ele assegura que apenas as partes autorizadas possam aceder aos dados, mesmo em caso de roubo.

![Img](https://protonmail.com/blog/wp-content/uploads/2015/05/bob-alice.jpg)
> *Figura de End-to-End encryption Protocol*


# L2TP/IPSec (Internet Protocol Security)
L2TP/IPSec é um grupo de protocols usados para criar uma conexão encriptada entre dispositivos na camada 3 do modelo OSI (camada de rede), ajudando a mander a dada enviada na network segura.

IPSec consegue manter uma conexão segura encriptando todas as mensagens para que apenas as partes autorizadas as possam compreender. 

![Img](https://web.mit.edu/rhel-doc/4/RH-DOCS/rhel-sg-en-4/figs/rhl-common/networkconfig/n-t-n-ipsec-diagram.png)
> *Figura de Internet Protocol Security Protocol*


# PPTP (Point-to-Point Tunneling Protocol)
O Point-to-Point Tunneling Protocol (PPTP) é um protocol VPN que permite fazer redes seguras que podem ser acedidas através da Internet, permitindo aos clientes aceder a uma rede a partir de um local remoto. Isto é útil para pessoas que precisam de se ligar a outra rede a partir de um local para outro local.

O PPTP é preferido em relação a outros protocolos VPN porque é mais rápido e é compatível com dispositivos moveis



# SSL/TLS (Secure Socket Layer)
O protocolo SSL (Secure Socket Layer) é um protocolo de segurança usado para proteger as comunicações entre um navegador e um servidor. Ele criptógrafa os dados transmitidas entre o client e o servidor para que não possam ser lidas por terceiros (MITM).

Um website com SSL/TLS têm `https` na url, enquanto sites sem SSL/TLS têm `http`.

O SSL só pode ser implementado em websites que possuam um certificado SSL. Um certificado SSL é como um cartão de identificação que prova que alguém é quem diz ser. Os certificados SSL são armazenados e exibidos pelo servidor destinatário.

![Img](https://www.cloudflare.com/img/learning/security/glossary/what-is-ssl/http-vs-https.svg)
> *Figura de HTTP (Hypertext Transfer Protocol) vs HTTPS (Hypertext Transfer Protocol Secure)*


# Proxy
Um proxy server é um servidor que atua como intermediário para um cliente e para a Internet,
fazendo que o IP de origem (o ip do cliente) seja trocado para o ip do proxy server.

O cliente conhece o endereço do proxy server, e quando fizer alguma pedido a Internet,
esse pedido será encaminhado para o proxy server, que ira receber e fazer ele o pedido a Internet,
quando o proxy server obter alguma resposta do pedido, ira encaminhar essa resposta ao cliente.

![Img](https://www.ibm.com/docs/en/SSPREK_10.0.2/com.ibm.isva.doc/wrp_config/graphics/proxy-prot-supp.png)
> *Figura Proxy Protocol Network Flow (pas-thru with SSL)*

## MITM (*Man In The Middle*)
Um ataque **Man In The Middle** é um tipo de ataque de espionagem, em que um computador (atacante) se meta no meio de uma comunicação de 2 computadores. Depois de se inserirem no "meio" da comunicação, o computador (atacante) finge ser um dos participantes da comunicação. Isto permite que o atacante receba informações de qualquer das partes.

Este ataque é bastante semelhante a um proxy server, a diferença é que o cliente a comunicar com outro computador não tenciona que alguém se meta no meio da comunicação e começa a sniffar ou até modificar os pacotes da comunicação dos 2 computadores.
![Img](https://i.imgur.com/9qd5UzB.png)

# Proxy VS VPN (Virtual Private Network)
Os servidores proxy e as redes privadas virtuais (VPNs) podem parecer semelhantes porque ambos encaminham pedidos e respostas através de um servidor externo. Ambos também permitem o acesso a websites com o IP de origem escondido. No entanto, as VPNs oferecem encriptação por todo o tráfego.