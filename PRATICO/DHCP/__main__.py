"""
Exemplo de um protocol que faz configurações automáticas
quando um cliente entra na network em python,
com a utilização de DHCP Protocol

@author: Martim Martins
@class: RCOM
"""
from typing import TypedDict

class MessagePayload(TypedDict):
	...


class DHCP:
	...