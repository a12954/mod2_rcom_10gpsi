"""
No exemplo a baixo, o HTTP proxy server utiliza socket protocol
para receber requests HTTP raw,
pelo client.

Este caso pratico ira mostar quanta informação
uma proxy consegue ter do cliente.

@class RCOM
@author Martim Martins <martim13artins13@gmail.com>
"""
import logging
import tornado.web
import tornado.httpclient

from enum import Enum
from typing import TypeVar, Dict, Any

T = TypeVar("T")

HOST = "0.0.0.0"
PORT = 8888
log = logging.getLogger("Proxy-Server")


async def request(url: str, **kw: Dict[Any, Any]) -> T:
    return await tornado.httpclient.AsyncHTTPClient().fetch(
        url, raise_error=False, **kw
    )


class HTTPMethods(Enum):
    GET: str = "GET"
    POST: str = "POST"
    CONNECT: str = "CONNECT"


class MessageType(Enum):
    NULL: bytes = b""


class Proxy(tornado.web.RequestHandler):
    SUPPORTED_METHODS = [x.name.upper() for x in HTTPMethods]

    async def connect(self):
        log.info(self.request.body)

    async def get(self):
        if self.request.body == MessageType.NULL:
            return

        if "Proxy-Connection" in self.request.headers:
            del self.request.headers["Proxy-Connection"]

        log.info(self.request.body)
        req = await request(
            self.request.uri,
            method=self.request.method,
            body=self.request.body or None,
            headers=self.request.headers,
            follow_redirects=False,
            allow_nonstandard_methods=True,
        )

        if req.error and not isinstance(req.error, tornado.httpclient.HTTPError):
            self.set_status(500)
            self.write(f"Internal server error:\n{req.error}")

        if req.body:
            self.set_header("Content-Length", len(req.body))
            self.write(req.body)

    async def post(self) -> None:
        # Post é exatamente igual ao get
        # mas o http method muda.
        await self.get()


if __name__ == "__main__":
    # Configurar logging
    logging.basicConfig(
        level=logging.DEBUG,
        format="[%(asctime)s] [%(levelname)s] [%(name)s] - %(message)s",
    )

    app = tornado.web.Application(
        [
            (r".*", Proxy),
        ]
    )
    app.listen(PORT, HOST)
    tornado.ioloop.IOLoop.instance().start()
