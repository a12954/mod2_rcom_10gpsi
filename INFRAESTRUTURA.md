# Projeto Infraestrutura de rede
Iremos fazer o projeto de infraestrutura de rede do [DigitalOcean](https://www.digitalocean.com/).

A Digital Ocean é uma plataforma de computação em nuvem que fornece infraestrutura como serviço, dando aos programadores e às outras empresas uma forma simples de criar e gerir as suas aplicações.
Ela oferece compute instances, armazenamento de informação e banda larga aos seus clientes.

## Switch ToR 
Foi escolhido para switch tor, o [Cisco Nexus 9332 ACI Leaf Switch w 32P 40G QSFP](https://www.connection.com/product/cisco-nexus-9332-aci-leaf-switch-w-32p-40g-qsfp/n9k-c9332pq/18118613), ele contem 36 portas, este switch permite até 2.56 terabits por segundo de bandwidth interna.

Foi escolhido este router em especifico porque é barato, têm 36 portas, e permite spine-leaf architecture, que é necessário para os servidores conseguirem comunicarem entre si.

## Servidor de Rack
Foi escolhido para servidor de rack, o [Dell PowerEdge R6525](https://www.dell.com/pt/empresas/p/poweredge-r6525/pd), com 32 ranhuras RDIMM DDR4, com 12 unidades SAS/SATA/NVMe, CPU [AMD EPYC™ de 3.ª Geração](https://ark.intel.com/content/www/us/en/ark/products/120496/intel-xeon-platinum-8180-processor-38-5m-cache-2-50-ghz.html) com até **64** núcleos por processador e com sistema operativo [Ubuntu 18.04.6 LTS](https://releases.ubuntu.com/18.04/).

Escolhemos este servidor porque suporta muita memoria ram e contem muitos núcleos por processador.


## Rack
O rack que contirá os servidores e swich ToR será o [server rack without doors](https://www.rackmountsolutions.net/kendall-howard-3180-3-001-37-37u-linier-open-frame-server-rack-no-doors-36-depth/), pois tem 37 rows, ou seja, pode conter 1 switch ToR e 36 switches, que são a quantidade de portas que o switch ToR tem.

## Sistema de Refrigereção
O sistema de refrigeração dos servidores será constituido pelos [kits ventilação rack 4 ventoinhas](https://cablematic.com/pt/produtos/kit-ventilacao-rack-19-1u-4-ventoinhas-de-120mm-rackmatic-RK064/?cr=EUR&ct=PT#extra_product_info).