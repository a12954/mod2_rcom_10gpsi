# Modelo OSI
O modelo OSI retrata sete camadas usadas por computadores para comunicar numa rede.
Este modelo de rede foi o primeiro a tornar-se uma norma, ou standard.
O modelo moderno não é basiado em OSI, mas sim no modelo mais simples TCP/IP. Contudo, o modelo OSI ainda é muito usado, pois ele ajuda a perceber como as redes funcionam e, consequentemente, a corrigir problemas.

![Img](https://i.imgur.com/16y347r_d.webp?maxwidth=760&fidelity=grand)
> *Figura que mostra todas as camadas do modelo OSI*

## 7. Aplicação
A camada aplicação fornece protocolos que permitem enviar e receber informação e apresentar dados significativos aos utilizadores.

Alguns exemplos de protocolos da camada de aplicação são o HTTP (Hypertext Transfer Protocol), FTP (File Transfer Protocol) e DNS (Domain Name System).

> **Application Layer é também chamada de Desktop Layer** 

## 6. Presentation
A camada de apresentação faz uma formatação dos dados para a *camada 7 aplicação* e encripta os dados.
A formatação dos dados é necessária para existir compatibilidade entre dispositivos.

## 5. Session
A camada da sessão cria canais de comunicação, chamados sessões, entre computadores. É responsável pela abertura das sessões, garantido que elas permanecem abertas e funcionais enquanto os dados são transferidos, e fechando-as quando a comunicação termina. A camada da sessão pode também definir pontos de controlo durante uma transferência de dados se a sessão for interrompida, os dispositivos podem retomar a transferência de dados a partir do último ponto de controlo.

Alguns exemplos de protocolos na camada de sessão são L2TP (Layer Two Tunneling Protocol), RTCP (Remote Transport Control Protocol), H.245 (Control Channel Protocol) e Socks

![Img](https://media.geeksforgeeks.org/wp-content/uploads/computer-network-osi-model-layers-session.png)
> *Figura a mostrar um exemplo de uma sessão entre 2 computadores*

## 4. Transport
A camada de transporte fornece entrega da mensagem completa de E2E (end-to-end) para comunicações fiáveis. Os dados na camada de transporte são referidos como segmentos.
A camada de transporte também fornece o reconhecimento do sucesso da transmissão de dados e retransmite os dados se for encontrado um erro. 

Exemplos de protocolos na camada 4 são o TCP (Protocolo de Controlo de Transmissão) e o UDP (Protocolo de Datagramas de Utilizador).

## 3. Network
A camada de rede funciona para a transmissão de dados de um host para o outro localizado em redes diferentes. Também se ocupa do encaminhamento de pacotes, ou seja, seleciona o caminho mais curto para transmitir o pacote.
Os endereços IP do remetente e do receptor são colocados no cabeçalho pela camada de rede.

Exemplos de protocolos na camada de rede são, IPv4/IPv6 (Internet Protocol), IPSec (Internet Protocol Secure)

## 2. Data Link
Esta camada é responsável pela conexão entre dois nodes físicos duma rede e por detetar e, se desejado, corrigir erros nos mesmos.

A camada de ligação de dados está dividida em duas subcamadas:  
- Logical Link Control (LLC)
- Media Access Control (MAC)

### LLC
Identifica protocolos de rede e deteta erros.

### MAC
Usa endereços MAC para conectar nodes e definir permissões para transmitir/receber informação

O endereço MAC do destinatário é obtido colocando um pedido ARP(Address Resolution Protocol) a perguntar "Quem tem esse endereço IP?" e o host que representa esse IP responderá com o seu endereço MAC.

Alguns exemplos de protocols na camada de ligação de dados são Ethernet *Local area network*, Point-to-Point Protocol (PPP) *Dual node*.

> **Switch e Bridge são dispositivios da camada ligação de dados.**

## 1. Physical
A camada mais baixa do modelo de referência OSI é a camada física. É responsável pela ligação física real entre os dispositivos. A camada física contém informação sob a forma de bits. É responsável pela transmissão de bits individuais de um node para o outro. Ao receber dados, esta camada irá receber o sinal recebido e convertê-lo em 0s e 1s e enviá-los para a camada Data Link, que irá voltar a juntar a moldura. 

> **Hub, Repeater, Modem, Cables são dispositivos camada física.**

# Modelo TCP/IP
Antes da internet se tornar tão popular, os protocolos de comunicação mais importantes eram TCP/IP, NETBEUI, IPX/SPX, Xerox Network System (XNS)  e o Apple Talk, fazendo assim que muitos dispositivos não se conseguiam comunicar, o protocolo TCP/IP expandiu-se e tornando-se actualmente o protocolo padrão de comunicação.

O TCP/IP (Transmission Control Protocol/Internet Protocol) representa um conjunto de protocols que permitem um conjunto de equipamentos quem constituem uma rede possam comunicar entre si.

![Img](https://paginas.fe.up.pt/~mrs01003/TCP_IP_ficheiros/image001.gif)
> *Diagrama que mostra as diferenças entre modelo OSI e modelo TCP/IP*